package models
import ( "time" )
type BankMasterList struct {
    Id         int64 `gorm:"primaryKey"`
    BankCode   string
    IfscPrefix string
    Mode string
    IMan string
    EMan string
    XMan string
    Neftrtgs string
    MaxIntBank string
    MaxIMan string
    MaxEMan string
    MaxXMan string
    MaxNeftrtgs string
    MinIntBank string
    MinIMan string
    MinEMan string
    MinXMan string
    MinNeftrtgs string
    BankToggle string
    BankShortName string
    AccountNumberLength string
    NMan string
    MaxNMan string
    MinNMan string
    KMan string `sql:"DEFAULT:N"`
    MinKMan string `sql:"DEFAULT:1"`
    MaxKMan string `sql:"DEFAULT:99999"`
    EMandate string `sql:"DEFAULT:N"`
    UpiEnabled bool `sql:"DEFAULT:false"`
    createdAt time.Time
    updatedAt time.Time
}

