package controllers

import (
	//"encoding/json"
	"net/http"
	"bankmasterlist/src/models"
	"gorm.io/driver/mysql"
        "gorm.io/gorm"

    "fmt"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm/logger"
	//"net/http/httputil"
)

func Show(c echo.Context) error {
        dsn := "root:root@tcp(127.0.0.1:3306)/testdb?charset=utf8mb4&parseTime=True&loc=Local"
		db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{ Logger: logger.Default.LogMode(logger.Info),})
		db.Migrator().CreateTable(&models.BankMasterList{})
		if err!= nil{
			fmt.Println("err")
		}
		db.Model(&models.BankMasterList{}).Create(map[string]interface{}{
			"BankCode": "SBIN001675", "IfscPrefix": "SBIN",
		  })
		var bank_master_list []models.BankMasterList
		db.Find(&bank_master_list)
		fmt.Printf("%s\n",bank_master_list)
	    return c.JSON(http.StatusOK, bank_master_list)
}

