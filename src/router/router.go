package router

import (
	"net/http"

	"bankmasterlist/src/controllers"

	"github.com/labstack/echo/v4"
)
func New() *echo.Echo{
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.GET("/show", controllers.Show )
	return e
}
